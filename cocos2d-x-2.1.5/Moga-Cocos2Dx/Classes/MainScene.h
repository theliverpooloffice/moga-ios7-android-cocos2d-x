#ifndef __MAIN_SCENE_H__
#define __MAIN_SCENE_H__

#include "cocos2d.h"

class MainScene : public cocos2d::CCLayerColor
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();

    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();
    
    void update(float fDelta);

    void createSprites();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(MainScene);

private:

    cocos2d::CCSprite       * button_A,
                            * button_B,
                            * button_X,
                            * button_Y,
                            * nub_Right,
                            * nub_Left,
                            * button_Up,
                            * button_Left,
                            * button_Right,
                            * button_Down,
                            * button_Start,
                            * button_Select,
                            * button_L1,
                            * button_R1,
                            * button_L2,
                            * button_R2,
                            * led_1,
                            * led_2,
                            * led_3,
                            * led_4,
                            * overlay;
    
    cocos2d::CCLabelBMFont  * text_AxisX,
                            * text_AxisY,
                            * text_AxisZ,
                            * text_ButtonB,
                            * text_ButtonY,
                            * text_ButtonA,
                            * text_ButtonX,
                            * text_ButtonUp,
                            * text_ButtonRight,
                            * text_ButtonLeft,
                            * text_ButtonDown,
                            * text_ButtonL1,
                            * text_ButtonR1,
                            * text_ButtonL2,
                            * text_ButtonR2,
                            * text_AxisRZ;
    
    bool displayStats = false;
};

#endif // __MAIN_SCENE_H__
