#include "MainScene.h"
#include "CCMogaController.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

USING_NS_CC;

#define COCOS_DEBUG 1

//using namespace Moga;

CCScene* MainScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    MainScene *layer = MainScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MainScene::init()
{
    //////////////////////////////
    // 1. super init first
    if (!CCLayerColor::initWithColor( ccc4(39,41,41,255)))
    {
        return false;
    }
    
    this->setTouchEnabled(true);
    
    this->createSprites();
    
    // Init the Controller and Get the Player Index.
    switch (Moga::CCMogaController::shared()->getPlayerIndex())
    {
        case 0:
            led_1->setVisible(true);
            break;
        case 1:
            led_2->setVisible(true);
            break;
        case 2:
            led_3->setVisible(true);
            break;
        case 3:
            led_4->setVisible(true);
            break;
        default:
            //Do Nothing
            break;
    }

    scheduleUpdate();
    
    CCLOG("Initialized scene");
    
    return true;
}

void MainScene::update(float fDelta)
{
    if (Moga::CCMogaController::shared()->getControllerState(Moga::STATE_CONNECTION) == Moga::ACTION_CONNECTED)
    {
        switch (Moga::CCMogaController::shared()->getPlayerIndex())
        {
            case 0:
                led_1->setVisible(true);
                break;
            case 1:
                led_2->setVisible(true);
                break;
            case 2:
                led_3->setVisible(true);
                break;
            case 3:
                led_4->setVisible(true);
                break;
            default:
                //Do Nothing
                break;
        }
        
        float aButton = Moga::CCMogaController::shared()->getButtonValue(Moga::KEYCODE_BUTTON_A);
        button_A->setOpacity(aButton * 255);
        char aString[5];
        sprintf(aString, "%.02f", aButton);
        text_ButtonA->setString(aString);
    
        float bButton = Moga::CCMogaController::shared()->getButtonValue(Moga::KEYCODE_BUTTON_B);
        button_B->setOpacity(bButton * 255);
        char bString[5];
        sprintf(bString, "%.02f", bButton);
        text_ButtonB->setString(bString);
    
        float xButton = Moga::CCMogaController::shared()->getButtonValue(Moga::KEYCODE_BUTTON_X);
        button_X->setOpacity(xButton * 255);
        char xString[5];
        sprintf(xString, "%.02f", xButton);
        text_ButtonX->setString(xString);
    
        float yButton = Moga::CCMogaController::shared()->getButtonValue(Moga::KEYCODE_BUTTON_Y);
        button_Y->setOpacity(yButton * 255);
        char yString[5];
        sprintf(yString, "%.02f", yButton);
        text_ButtonY->setString(yString);
        
        float l1Button = Moga::CCMogaController::shared()->getButtonValue(Moga::KEYCODE_BUTTON_L1);
        button_L1->setOpacity(l1Button * 255);
        char l1String[5];
        sprintf(l1String, "%.02f", l1Button);
        text_ButtonL1->setString(l1String);
    
        float r1Button = Moga::CCMogaController::shared()->getButtonValue(Moga::KEYCODE_BUTTON_R1);
        button_R1->setOpacity(r1Button * 255);
        char r1String[5];
        sprintf(r1String, "%.02f", r1Button);
        text_ButtonR1->setString(r1String);
    
        float upButton = Moga::CCMogaController::shared()->getButtonValue(Moga::KEYCODE_DPAD_UP);
        button_Up->setOpacity(upButton * 255);
        char upString[5];
        sprintf(upString, "%.02f", upButton);
        text_ButtonUp->setString(upString);
    
        float downButton = Moga::CCMogaController::shared()->getButtonValue(Moga::KEYCODE_DPAD_DOWN);
        button_Down->setOpacity(downButton * 255);
        char downString[5];
        sprintf(downString, "%.02f", downButton);
        text_ButtonDown->setString(downString);
    
        float leftButton = Moga::CCMogaController::shared()->getButtonValue(Moga::KEYCODE_DPAD_LEFT);
        button_Left->setOpacity(leftButton * 255);
        char leftString[5];
        sprintf(leftString, "%.02f", leftButton);
        text_ButtonLeft->setString(leftString);
    
        float rightButton = Moga::CCMogaController::shared()->getButtonValue(Moga::KEYCODE_DPAD_RIGHT);
        button_Right->setOpacity(rightButton * 255);
        char rightString[5];
        sprintf(rightString, "%.02f", rightButton);
        text_ButtonRight->setString(rightString);
        
        // Triggers ---------------
        float l2 = Moga::CCMogaController::shared()->getAxisValue(Moga::AXIS_LTRIGGER);
        float r2 = Moga::CCMogaController::shared()->getAxisValue(Moga::AXIS_RTRIGGER);
        
        button_L2->setOpacity(abs(l2 * 255));
        char l2String[5];
        sprintf(l2String, "%.02f", l2);
        text_ButtonL2->setString(l2String);
        
        button_R2->setOpacity(abs(r2 * 255));
        char r2String[5];
        sprintf(r2String, "%.02f", r2);
        text_ButtonR2->setString(r2String);
        
        // Nubs -------------------
        float mult = 32.0f;
        
        // LEFT NUB
        float lx = Moga::CCMogaController::shared()->getAxisValue(Moga::AXIS_X);
        float ly = Moga::CCMogaController::shared()->getAxisValue(Moga::AXIS_Y);
        
        nub_Left->setVisible(true);
        
        // Left Visual Position
        if (lx == 0 && ly == 0)
        {
            nub_Left->setVisible(false);
        }
        else
        {
            nub_Left->setVisible(true);
            nub_Left->setPosition(ccp(lx * mult, ly * mult));
        }
        
        char xAxisString[5];
        sprintf(xAxisString, "%.02f", lx);
        text_AxisX->setString(xAxisString);
        char yAxisString[5];
        sprintf(yAxisString, "%.02f", ly);
        text_AxisY->setString(yAxisString);
        
        // RIGHT NUB
        float rx = Moga::CCMogaController::shared()->getAxisValue(Moga::AXIS_Z);
        float ry = Moga::CCMogaController::shared()->getAxisValue(Moga::AXIS_RZ);

        nub_Right->setVisible(true);
        
        if (rx == 0 && ry == 0)
        {
            nub_Right->setVisible(false);
        }
        else
        {
            nub_Right->setVisible(true);
            nub_Right->setPosition(ccp(rx * mult, ry * mult));
        }
        
        char zAxisString[5];
        sprintf(zAxisString, "%.02f", rx);
        text_AxisZ->setString(zAxisString);
        char rzAxisString[5];
        sprintf(rzAxisString, "%.02f", ry);
        text_AxisRZ->setString(rzAxisString);
    }
    else
    {
        led_1->setVisible(false);
        led_2->setVisible(false);
        led_3->setVisible(false);
        led_4->setVisible(false);
    }
}

void MainScene::ccTouchesBegan(CCSet* touches, CCEvent* event)
{
    if (!displayStats)
    {
        overlay->setVisible(true);
        text_AxisX->setVisible(true);
        text_AxisY->setVisible(true);
        text_AxisZ->setVisible(true);
        text_ButtonB->setVisible(true);
        text_ButtonY->setVisible(true);
        text_ButtonA->setVisible(true);
        text_ButtonX->setVisible(true);
        text_ButtonUp->setVisible(true);
        text_ButtonRight->setVisible(true);
        text_ButtonLeft->setVisible(true);
        text_ButtonDown->setVisible(true);
        text_ButtonL1->setVisible(true);
        text_ButtonR1->setVisible(true);
        text_ButtonL2->setVisible(true);
        text_ButtonR2->setVisible(true);
        text_AxisRZ->setVisible(true);
        displayStats = true;
    }
    else
    {
        overlay->setVisible(false);
        text_AxisX->setVisible(false);
        text_AxisY->setVisible(false);
        text_AxisZ->setVisible(false);
        text_ButtonB->setVisible(false);
        text_ButtonY->setVisible(false);
        text_ButtonA->setVisible(false);
        text_ButtonX->setVisible(false);
        text_ButtonUp->setVisible(false);
        text_ButtonRight->setVisible(false);
        text_ButtonLeft->setVisible(false);
        text_ButtonDown->setVisible(false);
        text_ButtonL1->setVisible(false);
        text_ButtonR1->setVisible(false);
        text_ButtonL2->setVisible(false);
        text_ButtonR2->setVisible(false);
        text_AxisRZ->setVisible(false);
        displayStats = false;
    }
}

void MainScene::createSprites()
{
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    
    float offsetX = 0;
    float offsetY = 0;
    
    CCSprite * iPhone5background = CCSprite::create("backgroundPadding.png");
    iPhone5background->setPosition(ccp(visibleSize.width/2, visibleSize.height/2));
    addChild(iPhone5background);
    
    CCSprite * background = CCSprite::create("background.png");
    background->setPosition(ccp(visibleSize.width/2, visibleSize.height/2));
    addChild(background);
    
    overlay = CCSprite::create("backgroundOverlay.png");
    overlay->setPosition(ccp(visibleSize.width/2,visibleSize.height/2));
    addChild(overlay);
    
    CCSprite * logo = CCSprite::create("mogaLogo.png");
    logo->setAnchorPoint(ccp(0,1));
    logo->setPosition(ccp(visibleSize.width/10 * 0.5, visibleSize.height/10 * 9.5));
    addChild(logo);
    
    button_A = CCSprite::create("highlight_a.png");
    button_A->setPosition(ccp(visibleSize.width / 10 * 6.75 + offsetX, visibleSize.height/ 10 * 5 + offsetY));
    button_A->setOpacity(0);
    addChild(button_A);
    
    button_B = CCSprite::create("highlight_b.png");
    button_B->setPosition(ccp(visibleSize.width / 10 * 7.06 + offsetX, visibleSize.height/ 10 * 5.51 + offsetY));
    button_B->setOpacity(0);
    addChild(button_B);
    
    button_X = CCSprite::create("highlight_x.png");
    button_X->setPosition(ccp(visibleSize.width / 10 * 6.43 + offsetX, visibleSize.height/ 10 * 5.51 + offsetY));
    button_X->setOpacity(0);
    addChild(button_X);
    
    button_Y = CCSprite::create("highlight_y.png");
    button_Y->setPosition(ccp(visibleSize.width / 10 * 6.75 + offsetX, visibleSize.height/ 10 * 6.09 + offsetY));
    button_Y->setOpacity(0);
    addChild(button_Y);
    
    CCNode * nub_RightCenter = CCNode::create();
    nub_RightCenter->setPosition(ccp(visibleSize.width/10 * 6.34 + offsetX, visibleSize.height/10 * 3.37 + offsetY));
    addChild(nub_RightCenter);
    
    nub_Right = CCSprite::create("highlight_nub.png");
    nub_Right->setVisible(false);
    nub_RightCenter->addChild(nub_Right);
    
    CCNode * nub_LeftCenter = CCNode::create();
    nub_LeftCenter->setPosition(ccp(visibleSize.width/10 * 3.195 + offsetX, visibleSize.height/10 * 6.03 + offsetY));
    addChild(nub_LeftCenter);
    
    nub_Left = CCSprite::create("highlight_nub.png");
    nub_Left->setVisible(false);
    nub_LeftCenter->addChild(nub_Left);
    
    button_Up = CCSprite::create("highlight_up.png");
    button_Up->setPosition(ccp(visibleSize.width/10 * 3.55 + offsetX, visibleSize.height/10 * 4.06 + offsetY));
    button_Up->setOpacity(0);
    addChild(button_Up);
    
    button_Left = CCSprite::create("highlight_left.png");
    button_Left->setPosition(ccp(visibleSize.width/10 * 3.28 + offsetX, visibleSize.height/10 * 3.54 + offsetY));
    button_Left->setOpacity(0);
    addChild(button_Left);
    
    button_Right = CCSprite::create("highlight_right.png");
    button_Right->setPosition(ccp(visibleSize.width/10 * 3.85 + offsetX, visibleSize.height/10 * 3.54 + offsetY));
    button_Right->setOpacity(0);
    addChild(button_Right);
    
    button_Down = CCSprite::create("highlight_down.png");
    button_Down->setPosition(ccp(visibleSize.width/10 * 3.55 + offsetX, visibleSize.height/10 * 3.02 + offsetY));
    button_Down->setOpacity(0);
    addChild(button_Down);
    
    button_L1 = CCSprite::create("highlight_l1.png");
    button_L1->setPosition(ccp(visibleSize.width/10 * 3.4 + offsetX, visibleSize.height/10 * 7.71 + offsetY));
    button_L1->setOpacity(0);
    addChild(button_L1);
    
    button_R1 = CCSprite::create("highlight_r1.png");
    button_R1->setPosition(ccp(visibleSize.width/10 * 6.53 + offsetX, visibleSize.height/10 * 7.71 + offsetY));
    button_R1->setOpacity(0);
    addChild(button_R1);
    
    button_L2 = CCSprite::create("highlight_l2.png");
    button_L2->setPosition(ccp(visibleSize.width/10 * 3.4 + offsetX, visibleSize.height/10 * 8.59 + offsetY));
    button_L2->setOpacity(0);
    addChild(button_L2);
    
    button_R2 = CCSprite::create("highlight_r2.png");
    button_R2->setPosition(ccp(visibleSize.width/10 * 6.5 + offsetX, visibleSize.height/10 * 8.59 + offsetY));
    button_R2->setOpacity(0);
    addChild(button_R2);
    
    led_1 = CCSprite::create("led.png");
    led_1->setPosition(ccp(visibleSize.width/10 * 5.81 + offsetX, visibleSize.height/10 * 6.1 + offsetY));
    led_1->setVisible(false);
    addChild(led_1);
    
    led_2 = CCSprite::create("led.png");
    led_2->setPosition(ccp(visibleSize.width/10 * 5.81 + offsetX, visibleSize.height/10 * 6.3 + offsetY));
    led_2->setVisible(false);
    addChild(led_2);
    
    led_3 = CCSprite::create("led.png");
    led_3->setPosition(ccp(visibleSize.width/10 * 5.81 + offsetX, visibleSize.height/10 * 6.5 + offsetY));
    led_3->setVisible(false);
    addChild(led_3);
    
    led_4 = CCSprite::create("led.png");
    led_4->setPosition(ccp(visibleSize.width/10 * 5.81 + offsetX, visibleSize.height/10 * 6.7 + offsetY));
    led_4->setVisible(false);
    addChild(led_4);
    
    text_AxisX = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_AxisX->setScale(0.5f);
    text_AxisX->setAnchorPoint(ccp(0, 1));
    text_AxisX->setPosition(ccp(visibleSize.width/10 * 4.14, visibleSize.height/10 * 5.94));
    addChild(text_AxisX);
    
    text_AxisY = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_AxisY->setRotation(-90);
    text_AxisY->setScale(0.5f);
    text_AxisY->setAnchorPoint(ccp(0, 1));
    text_AxisY->setPosition(ccp(visibleSize.width/10 * 3.25, visibleSize.height/10 * 3.8));
    addChild(text_AxisY);
    
    text_AxisZ = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_AxisZ->setAnchorPoint(ccp(0, 1));
    text_AxisZ->setScale(0.5f);
    text_AxisZ->setPosition(ccp(visibleSize.width/10 * 7.3, visibleSize.height/10 * 3.3));
    addChild(text_AxisZ);
    
    text_ButtonB = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonB->setAnchorPoint(ccp(0, 1));
    text_ButtonB->setScale(0.5f);
    text_ButtonB->setPosition(ccp(visibleSize.width/10 * 8.1, visibleSize.height/10 * 5.33));
    addChild(text_ButtonB);
    
    text_ButtonY = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonY->setAnchorPoint(ccp(0, 1));
    text_ButtonY->setScale(0.5f);
    text_ButtonY->setPosition(ccp(visibleSize.width/10 * 7.79, visibleSize.height/10 * 6.09));
    addChild(text_ButtonY);
    
    text_ButtonA = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonA->setAnchorPoint(ccp(0, 1));
    text_ButtonA->setScale(0.5f);
    text_ButtonA->setPosition(ccp(visibleSize.width/10 * 5.37, visibleSize.height/10 * 4.83));
    addChild(text_ButtonA);
    
    text_ButtonX = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonX->setAnchorPoint(ccp(0, 1));
    text_ButtonX->setScale(0.5f);
    text_ButtonX->setPosition(ccp(visibleSize.width/10 * 5.05, visibleSize.height/10 * 5.52));
    addChild(text_ButtonX);
    
    text_ButtonUp = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonUp->setAnchorPoint(ccp(1, 1));
    text_ButtonUp->setScale(0.5f);
    text_ButtonUp->setPosition(ccp(visibleSize.width/10 * 4.9, visibleSize.height/10 * 4.23));
    addChild(text_ButtonUp);
    
    text_ButtonRight = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonRight->setAnchorPoint(ccp(1, 1));
    text_ButtonRight->setScale(0.5f);
    text_ButtonRight->setPosition(ccp(visibleSize.width/10 * 5.25, visibleSize.height/10 * 3.51));
    addChild(text_ButtonRight);
    
    text_ButtonLeft = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonLeft->setAnchorPoint(ccp(0, 1));
    text_ButtonLeft->setScale(0.5f);
    text_ButtonLeft->setPosition(ccp(visibleSize.width/10 * 1.84, visibleSize.height/10 * 3.35));
    addChild(text_ButtonLeft);
    
    text_ButtonDown = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonDown->setAnchorPoint(ccp(0, 1));
    text_ButtonDown->setScale(0.5f);
    text_ButtonDown->setPosition(ccp(visibleSize.width/10 * 2.2, visibleSize.height/10 * 2.7));
    addChild(text_ButtonDown);
    
    text_ButtonL1 = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonL1->setAnchorPoint(ccp(0, 1));
    text_ButtonL1->setScale(0.5f);
    text_ButtonL1->setPosition(ccp(visibleSize.width/10 * 1.82, visibleSize.height/10 * 7.6));
    addChild(text_ButtonL1);
    
    text_ButtonR1 = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonR1->setAnchorPoint(ccp(1, 1));
    text_ButtonR1->setScale(0.5f);
    text_ButtonR1->setPosition(ccp(visibleSize.width/10 * 8.1, visibleSize.height/10 * 7.6));
    addChild(text_ButtonR1);
    
    text_ButtonL2 = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonL2->setAnchorPoint(ccp(0, 1));
    text_ButtonL2->setScale(0.5f);
    text_ButtonL2->setPosition(ccp(visibleSize.width/10 * 1.82, visibleSize.height/10 * 8.5));
    addChild(text_ButtonL2);
    
    text_ButtonR2 = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_ButtonR2->setAnchorPoint(ccp(1, 1));
    text_ButtonR2->setScale(0.5f);
    text_ButtonR2->setPosition(ccp(visibleSize.width/10 * 8.1, visibleSize.height/10 * 8.5));
    addChild(text_ButtonR2);
    
    text_AxisRZ = CCLabelBMFont::create("0.00", "bmpFont.fnt");
    text_AxisRZ->setRotation(-90);
    text_AxisRZ->setAnchorPoint(ccp(0, 1));
    text_AxisRZ->setScale(0.5f);
    text_AxisRZ->setPosition(ccp(visibleSize.width/10 * 6.4, visibleSize.height/10 * 1.15));
    addChild(text_AxisRZ);
    
    overlay->setVisible(false);
    text_AxisX->setVisible(false);
    text_AxisY->setVisible(false);
    text_AxisZ->setVisible(false);
    text_ButtonB->setVisible(false);
    text_ButtonY->setVisible(false);
    text_ButtonA->setVisible(false);
    text_ButtonX->setVisible(false);
    text_ButtonUp->setVisible(false);
    text_ButtonRight->setVisible(false);
    text_ButtonLeft->setVisible(false);
    text_ButtonDown->setVisible(false);
    text_ButtonL1->setVisible(false);
    text_ButtonR1->setVisible(false);
    text_ButtonL2->setVisible(false);
    text_ButtonR2->setVisible(false);
    text_AxisRZ->setVisible(false);
}