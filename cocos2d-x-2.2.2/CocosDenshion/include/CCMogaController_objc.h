/**
 A wrapper to the CCMogaController object.

 Requirements:
 - Firmware: OS 7.0 or greater
 - Files: CCMogaController.*
 - Frameworks: GameController
 */
@interface CCMogaController : NSObject
{
    int   connectionStatus_;
    BOOL   isPaused_;
}

/** returns the shared instance of the CCMogaController object */
+ (CCMogaController*)shared;

/** returns the state */
-(int) getControllerState:(int) state;
/** returns the controllers player id */
-(int) getPlayerIndex;
/** returns the number of controllers connected */
-(int) getControllerCount;
/** returns the state of controllers button */
-(float) getButtonValue:(int) keyCode;
/** returns the state of controllers axis */
-(float) getAxisValue:(int) axis;

/** Shuts down the shared game controller instance so that it can be reinitialised */
+(void) end;

@end