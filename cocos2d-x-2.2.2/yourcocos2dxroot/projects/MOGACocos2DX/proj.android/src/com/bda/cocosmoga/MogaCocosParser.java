package com.bda.cocosmoga;

import java.util.Arrays;

import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

//import android.annotation.SuppressLint;

public class MogaCocosParser {
	
	static MogaCocosParser 	mInstance;
	final float	[] 		mAxis 	= new float[AxisIndex.SIZE];
	final int[] 		mKey 	= new int[KeyIndex.SIZE];
	//final float[] mPrecision = new float[PrecisionIndex.SIZE];
	final int[] 		mState	= new int[StateIndex.SIZE];
	
	/*
	//Axis index for onHIDMotionEvent
	public static final int HAXIS_X = 0;
	public static final int HAXIS_Y = 1;
	public static final int HAXIS_Z = 2;
	public static final int HAXIS_RZ = 3;
	public static final int HAXIS_L2 = 4;
	public static final int HAXIS_R2 = 5;
	public static final int HAXIS_HAT_X = 6;
	public static final int HAXIS_HAT_Y = 7;
	
	final ControllerService mService;
	final int mSystemId;
	*/
	
	
	protected MogaCocosParser() {
		reset();
	}
	
	
	public static MogaCocosParser Instance() {
		if(mInstance == null) {
			mInstance = new MogaCocosParser();
		}
		return mInstance;
	}
	
	
	public void reset() {
		Arrays.fill(mAxis, 0);
		Arrays.fill(mKey, 0);
		Arrays.fill(mState, 0);
	}
	
	
	public boolean onKeyEvent(KeyEvent event) {
		
		int keyEvent = (event.getAction() == android.view.KeyEvent.ACTION_DOWN) ? 1 : 0;
		Log.d("JIMMY", "Button: " + event.getKeyCode() + " : " + keyEvent);
		switch (event.getKeyCode()) {
		case KeyEvent.KEYCODE_BUTTON_A:
			mKey[KeyIndex.KEYCODE_BUTTON_A] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_BUTTON_B:
			mKey[KeyIndex.KEYCODE_BUTTON_B] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_BUTTON_X:
			mKey[KeyIndex.KEYCODE_BUTTON_X] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_BUTTON_Y:
			mKey[KeyIndex.KEYCODE_BUTTON_Y] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_BUTTON_L1:
			mKey[KeyIndex.KEYCODE_BUTTON_L1] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_BUTTON_R1:
			mKey[KeyIndex.KEYCODE_BUTTON_R1] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_BUTTON_THUMBL:
			mKey[KeyIndex.KEYCODE_BUTTON_THUMBL] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_BUTTON_THUMBR:
			mKey[KeyIndex.KEYCODE_BUTTON_THUMBR] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_BUTTON_START:
			mKey[KeyIndex.KEYCODE_BUTTON_START] = keyEvent;
			return true;
		case android.view.KeyEvent.KEYCODE_BACK:
			mKey[KeyIndex.KEYCODE_BUTTON_SELECT] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_DPAD_LEFT:
			Log.d("JIMMY", "Dpad Moved");
			mKey[KeyIndex.KEYCODE_DPAD_LEFT] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_DPAD_DOWN:
			Log.d("JIMMY", "Dpad Moved");
			mKey[KeyIndex.KEYCODE_DPAD_DOWN] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			Log.d("JIMMY", "Dpad Moved");
			mKey[KeyIndex.KEYCODE_DPAD_RIGHT] = keyEvent;
			return true;
		case KeyEvent.KEYCODE_DPAD_UP:
			Log.d("JIMMY", "Dpad Moved");
			mKey[KeyIndex.KEYCODE_DPAD_UP] = keyEvent;
			return true;
		}
		
		return false;
	}
	
	
	public boolean onGenericMotionEvent(MotionEvent event) {
				
		mAxis[AxisIndex.AXIS_X] = event.getAxisValue(MotionEvent.AXIS_X);
		mAxis[AxisIndex.AXIS_Y] = -event.getAxisValue(MotionEvent.AXIS_Y); //Inverted to follow iOS Controller
		mAxis[AxisIndex.AXIS_Z] = event.getAxisValue(MotionEvent.AXIS_Z);
		mAxis[AxisIndex.AXIS_RZ] = -event.getAxisValue(MotionEvent.AXIS_RZ); //Inverted to follow iOS controller
		
		mAxis[AxisIndex.AXIS_LTRIGGER] = event.getAxisValue(MotionEvent.AXIS_GAS);
		mAxis[AxisIndex.AXIS_RTRIGGER] = event.getAxisValue(MotionEvent.AXIS_BRAKE);

		mKey[KeyIndex.KEYCODE_BUTTON_L2] = (mAxis[AxisIndex.AXIS_LTRIGGER] > 0.0) ? 1 : 0;
		mKey[KeyIndex.KEYCODE_BUTTON_R2] = (mAxis[AxisIndex.AXIS_RTRIGGER] > 0.0) ? 1 : 0;
		
		//Moga d-pad uses hat axis, keys need to be set here.
		mKey[KeyIndex.KEYCODE_DPAD_UP] = (event.getAxisValue(MotionEvent.AXIS_HAT_Y) == -1) ? 1 : 0;
		mKey[KeyIndex.KEYCODE_DPAD_DOWN] = (event.getAxisValue(MotionEvent.AXIS_HAT_Y)  == 1) ? 1 : 0;
		mKey[KeyIndex.KEYCODE_DPAD_LEFT] = (event.getAxisValue(MotionEvent.AXIS_HAT_X)  == -1) ? 1 : 0;
		mKey[KeyIndex.KEYCODE_DPAD_RIGHT] = (event.getAxisValue(MotionEvent.AXIS_HAT_X)  == 1) ? 1 : 0;
		
		if(mAxis[AxisIndex.AXIS_X] < 0.01 && mAxis[AxisIndex.AXIS_X] > -0.01){
			mAxis[AxisIndex.AXIS_X] = 0;
		}
		if(mAxis[AxisIndex.AXIS_Y] < 0.01 && mAxis[AxisIndex.AXIS_Y] > -0.01){
			mAxis[AxisIndex.AXIS_Y] = 0;
		}
		if(mAxis[AxisIndex.AXIS_Z] < 0.01 && mAxis[AxisIndex.AXIS_Z] > -0.01){
			mAxis[AxisIndex.AXIS_Z] = 0;
		}
		if(mAxis[AxisIndex.AXIS_RZ] < 0.01 && mAxis[AxisIndex.AXIS_RZ] > -0.01){
			mAxis[AxisIndex.AXIS_RZ] = 0;
		}
		
		return true;
	}

	/**
	 * Returns the state of the controller
	 * @param state
	 * @return
	 */
	public static int getControllerState(int state) {
		
		switch(state) {
			//TODO
		}
		
		return 0;
	}
    
    /**
     * Gets the current player index
     */
    public static int getPlayerIndex() {
    	return 1; //TODO
    }
    

    /**
     * Returns the number of connected controllers
     */
    public static int getControllerCount() {
    	return 1; //TODO
    }
    

    /**
     * Returns the press state of a button
     * @param keyCode
     * @return 1 if pressed, 0 if not
     */
    public static float getButtonValue(int keyCode) {
    	switch(keyCode)
    	{
    	case CCKeys.KEYCODE_BUTTON_A:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_A];
    	case CCKeys.KEYCODE_BUTTON_B:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_B];
    	case CCKeys.KEYCODE_BUTTON_X:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_X];
    	case CCKeys.KEYCODE_BUTTON_Y:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_Y];
    	case CCKeys.KEYCODE_BUTTON_L1:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_L1];
    	case CCKeys.KEYCODE_BUTTON_R1:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_R1];
    	case CCKeys.KEYCODE_BUTTON_L2:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_L2];
    	case CCKeys.KEYCODE_BUTTON_R2:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_R2];
    	case CCKeys.KEYCODE_BUTTON_THUMBL:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_THUMBL];
    	case CCKeys.KEYCODE_BUTTON_THUMBR:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_THUMBR];
    	case CCKeys.KEYCODE_BUTTON_SELECT:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_SELECT];
    	case CCKeys.KEYCODE_BUTTON_START:
			return Instance().mKey[KeyIndex.KEYCODE_BUTTON_START];
    	case CCKeys.KEYCODE_DPAD_LEFT:
    		return Instance().mKey[KeyIndex.KEYCODE_DPAD_LEFT];
    	case CCKeys.KEYCODE_DPAD_DOWN:
    		return Instance().mKey[KeyIndex.KEYCODE_DPAD_DOWN];
    	case CCKeys.KEYCODE_DPAD_RIGHT:
    		return Instance().mKey[KeyIndex.KEYCODE_DPAD_RIGHT];
    	case CCKeys.KEYCODE_DPAD_UP:
    		return Instance().mKey[KeyIndex.KEYCODE_DPAD_UP];
    	}
    	return 0;
    }
    

    /**
     * Get the current state of a given axis
     * @param axis
     * @return
     */
    public static float getAxisValue(int axis) {
    	switch(axis) {
    	case CCAxis.AXIS_LTRIGGER:
    		return Instance().mAxis[AxisIndex.AXIS_LTRIGGER];
    	case CCAxis.AXIS_RTRIGGER:
    		return Instance().mAxis[AxisIndex.AXIS_RTRIGGER];
    	case CCAxis.AXIS_X:
    		return Instance().mAxis[AxisIndex.AXIS_X];
    	case CCAxis.AXIS_Y:
    		return Instance().mAxis[AxisIndex.AXIS_Y];
    	case CCAxis.AXIS_Z:
    		return Instance().mAxis[AxisIndex.AXIS_Z];
    	case CCAxis.AXIS_RZ:
    		return Instance().mAxis[AxisIndex.AXIS_RZ];
    	}
    	return 0;
    }
    
    
    
	/*
	@Override
	void reset() {
		mControllerId = 1;
				
		Arrays.fill(mAxis, 0.0f);
		Arrays.fill(mKey, KeyEvent.ACTION_UP);
		Arrays.fill(mPrecision, 0.0f);
		Arrays.fill(mState, StateEvent.ACTION_FALSE);
		Arrays.fill(mAxisOld, 0.0f);
		Arrays.fill(mKeyOld, KeyEvent.ACTION_UP);
		Arrays.fill(mStateOld, StateEvent.ACTION_FALSE);
		
		mState[StateIndex.STATE_SUPPORTED_VERSION] = ControllerModel.MOGA_PRO;
		mState[StateIndex.STATE_SELECTED_VERSION] = mState[StateIndex.STATE_SUPPORTED_VERSION];
		mState[StateIndex.STATE_CONNECTION] = StateEvent.ACTION_CONNECTED;
	}
	 */
	
	static class CCKeys
    {
        static final int 	KEYCODE_BUTTON_A    	= 10,
        					KEYCODE_BUTTON_B    	= 11,
        					KEYCODE_BUTTON_X    	= 12,
        					KEYCODE_BUTTON_Y    	= 13,
        					KEYCODE_BUTTON_START    = 14,
        					KEYCODE_BUTTON_SELECT   = 15,
        					KEYCODE_BUTTON_L1       = 16,
        					KEYCODE_BUTTON_R1       = 17,
        					KEYCODE_BUTTON_L2       = 18,
        					KEYCODE_BUTTON_R2       = 19,
        					KEYCODE_BUTTON_THUMBL   = 20,
        					KEYCODE_BUTTON_THUMBR   = 21,
        					KEYCODE_DPAD_UP         = 22,
        					KEYCODE_DPAD_DOWN       = 23,
        					KEYCODE_DPAD_LEFT       = 24,
        					KEYCODE_DPAD_RIGHT      = 25;
    };
    
    static class CCAxis
    {
        static final int 	AXIS_X			= 0,
        					AXIS_Y			= 1,
        					AXIS_Z			= 11,
        					AXIS_RZ			= 14,
        					AXIS_LTRIGGER	= 17,
        					AXIS_RTRIGGER	= 18;
    };
    
    static class CCActions
    {
        static final int 	ACTION_CONNECTED	= 0,
        					ACTION_DISCONNECTED	= 1,
        					ACTION_CONNECTING	= 2;
    };
    
    static class CCStates
    {
        static final int	STATE_CONNECTION					= 10,
        					STATE_POWER_LOW                     = 11,
        					STATE_SUPPORTED_PRODUCT_VERSION     = 12,
        					STATE_CURRENT_PRODUCT_VERSION       = 13;
    };
    
	static class AxisIndex {
		static final int 	AXIS_X = 0,
							AXIS_Y = 1,
							AXIS_Z = 2,
							AXIS_RZ = 3,
							AXIS_LTRIGGER = 4,
							AXIS_RTRIGGER = 5,
							SIZE = 6;
	}
	
	
	static class KeyIndex {
		static final int 	KEYCODE_DPAD_UP 		= 0,
							KEYCODE_DPAD_DOWN 		= 1,
							KEYCODE_DPAD_LEFT 		= 2,
							KEYCODE_DPAD_RIGHT 		= 3,
							KEYCODE_BUTTON_A 		= 4,
							KEYCODE_BUTTON_B 		= 5,
							KEYCODE_BUTTON_X 		= 6,
							KEYCODE_BUTTON_Y 		= 7,
							KEYCODE_BUTTON_L1 		= 8,
							KEYCODE_BUTTON_R1 		= 9,
							KEYCODE_BUTTON_L2 		= 10,
							KEYCODE_BUTTON_R2 		= 11,
							KEYCODE_BUTTON_THUMBL 	= 12,
							KEYCODE_BUTTON_THUMBR 	= 13,
							KEYCODE_BUTTON_START 	= 14,
							KEYCODE_BUTTON_SELECT 	= 15,
							SIZE 					= 16;
	}
	
	
	static class StateIndex {
		static final int 	STATE_CONNECTION 		= 0,
							STATE_POWER_LOW 		= 1,
							STATE_SUPPORTED_VERSION = 2,
							STATE_SELECTED_VERSION 	= 3,
							SIZE 					= 4;
	}
	
}
