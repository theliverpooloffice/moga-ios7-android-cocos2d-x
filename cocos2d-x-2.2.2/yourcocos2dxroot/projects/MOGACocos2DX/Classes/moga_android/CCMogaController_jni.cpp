#include "jni.h"
#include "CCMogaController_jni.h"
#include "cocos2d.h"
#include "../platform/android/jni/JniHelper.h" //may need to move reference if class is in a different file
//#include <android/log.h>

USING_NS_CC;


namespace Moga
{
	extern "C"
	{
		JavaVM *jVM;
		JNIEnv *env;

		jclass jControllerClass;

		jmethodID get_controller_state;
		jmethodID get_player_index;
		jmethodID get_controller_count;
		jmethodID get_button_value;
		jmethodID get_axis_value;
        
        static CCMogaController_jni *instance;
        
        CCMogaController_jni* CCMogaController_jni::Instance()
        {
            if(instance == NULL)
            {
                instance = new CCMogaController_jni();
            }
            return instance;
        }
        
        
        CCMogaController_jni::CCMogaController_jni()
        {
            jni_init();
        }
        
        
        CCMogaController_jni::~CCMogaController_jni()
        {
            delete jControllerClass;
            delete instance;
            instance = NULL;
            jControllerClass = NULL;
        }

        
		void CCMogaController_jni::jni_init()
		{
            //get the jni envitorment from the current JavaVM.
            //Android has 1 constant JavaVM, so can't make a new one.
			jVM = JniHelper::getJavaVM();
            jVM->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_2);
            
            //find parser class
			jclass jController = env->FindClass("com/bda/cocosmoga/MogaCocosParser");
            if(jController == NULL)
            {
                CCLog("ERROR: Can't find MogaCocosParser");
                return;
            }
            
            //Create global reference to parser class so it doesn't get deleted
			jControllerClass = static_cast<jclass>(env->NewGlobalRef(jController));
            env->DeleteLocalRef(jController);

            //Set up links to method id's
			get_controller_state = env->GetStaticMethodID(jControllerClass, "getControllerState", "(I)I");
			get_player_index = env->GetStaticMethodID(jControllerClass, "getPlayerIndex", "()I");
			get_controller_count = env->GetStaticMethodID(jControllerClass, "getControllerCount", "()I");
			get_button_value = env->GetStaticMethodID(jControllerClass, "getButtonValue", "(I)F");
			get_axis_value = env->GetStaticMethodID(jControllerClass, "getAxisValue", "(I)F");
		}

        
        void CCMogaController_jni::jni_end()
        {
            delete instance;
            instance = NULL;
        }
        

		unsigned int CCMogaController_jni::jni_getControllerState(int state)
		{
            if(get_controller_state == NULL)
            {
                CCLog("ERROR: GetState Null ID!!!");
                return 0;
            }

			return env->CallStaticIntMethod(jControllerClass, get_controller_state, state);
		}

        
		unsigned int CCMogaController_jni::jni_getPlayerIndex()
		{
            if(get_player_index == NULL)
            {
                CCLog("ERROR: GetIndex Null ID!!!");
                return 0;
            }

			return env->CallStaticIntMethod(jControllerClass, get_player_index);
		}

        
		unsigned int CCMogaController_jni::jni_getControllerCount()
		{
            if(get_controller_count == NULL)
            {
                CCLog("ERROR: GetControllerCount Null ID!!!");
                return 0;
            }

			return env->CallStaticIntMethod(jControllerClass, get_controller_count);
		}

        
		float CCMogaController_jni::jni_getButtonValue(int keyCode)
		{
            if(get_button_value == NULL)
            {
                CCLog("ERROR: GetButtonValue Null ID!!!");
                return 0.0;
            }

			return env->CallStaticFloatMethod(jControllerClass, get_button_value, keyCode);
		}

        
		float CCMogaController_jni::jni_getAxisValue(int axis)
		{
            if(get_axis_value == NULL)
            {
                CCLog("ERROR: GetAxisValue Null ID!!!");
                return 0.0;
            }

			return env->CallStaticFloatMethod(jControllerClass, get_axis_value, axis);
		}
	}
}


