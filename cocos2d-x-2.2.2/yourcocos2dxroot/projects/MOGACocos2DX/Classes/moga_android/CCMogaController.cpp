#include "CCMogaController_jni.h"
#include <jni.h>
#include "cocos2d.h"
#include "CCMogaController.h"

USING_NS_CC;

namespace Moga {
    
    static CCMogaController *gameController;
    CCMogaController_jni *jniController;
    
    CCMogaController::CCMogaController()
    {
        jniController = CCMogaController_jni::Instance();
    }
    
    CCMogaController::~CCMogaController()
    {
        if(jniController != NULL)
        {
            jniController->jni_end();
        }
    }
    
    CCMogaController* CCMogaController::shared()
    {
        if (! gameController)
        {
            gameController = new CCMogaController();
        }
        
        return gameController;
    }
    
    void CCMogaController::end()
    {
        if (gameController)
        {
            delete gameController;
            gameController = NULL;
        }
    }
    
    unsigned int CCMogaController::getControllerState(int state)
    {
        return jniController->jni_getControllerState(state);
    }
    
    unsigned int CCMogaController::getPlayerIndex()
    {
        return jniController->jni_getPlayerIndex();
    }
    
    unsigned int CCMogaController::getControllerCount()
    {
        return jniController->jni_getControllerCount();
    }
    
    float CCMogaController::getButtonValue(int keyCode)
    {
        return jniController->jni_getButtonValue(keyCode);
    }
    
    float CCMogaController::getAxisValue(int axis)
    {
        return jniController->jni_getAxisValue(axis);
    }
    
} // endof namespace Moga
