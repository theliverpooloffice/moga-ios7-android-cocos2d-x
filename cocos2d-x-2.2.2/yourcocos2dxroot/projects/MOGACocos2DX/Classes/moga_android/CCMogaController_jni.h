#ifndef _GAME_CONTROLLER_JNI_H_
#define _GAME_CONTROLLER_JNI_H_

#include <jni.h>

namespace Moga
{
	class CCMogaController_jni
	{
        
        
	public:
        CCMogaController_jni();
        ~CCMogaController_jni();
        
        static CCMogaController_jni* Instance();
        
		void jni_init();

		void jni_end();

		unsigned int jni_getControllerState(int state);

		unsigned int jni_getPlayerIndex();

		unsigned int jni_getControllerCount();

		float jni_getButtonValue(int keyCode);

		float jni_getAxisValue(int axis);
	};
}
#endif
